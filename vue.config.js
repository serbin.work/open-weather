module.exports = {
    css: {
        loaderOptions: {
            css: {
                sourceMap: true
            },
            sass: {
                sourceMap: true
            }
        }
    }
}