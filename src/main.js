import Vue from 'vue'
import App from './App.vue'
import Notifications from 'vue-notification'
import axios from 'axios'
import moment from 'moment'
import _ from 'lodash'
import Vuelidate from 'vuelidate'
import VueCarousel from 'vue-carousel'

import router from './router'
import store from './store'

Vue.use(Notifications);
Vue.use(Vuelidate);
Vue.use(VueCarousel);

Vue.config.productionTip = false;
window.axios = axios;
window.openWeatherAPI = axios.create({
  baseURL: process.env.VUE_APP_API,
});
openWeatherAPI.interceptors.request.use(config => {
  config.params.APPID = process.env.VUE_APP_API_KEY;
  return config;
});
window._ = _;
window.moment = moment;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
