export default {
    methods: {
        getWeather(params) {
            return openWeatherAPI('/data/2.5/weather', {params})
                .then(({data}) => {
                    return data;
                })
        },
        getForecast(params) {
            return openWeatherAPI('/data/2.5/forecast', {params})
                .then(({data}) => {
                    data.list.forEach((item,index) => {
                        item.format = JSON.parse(JSON.stringify(this.get('format')));
                        item.currentDate = moment(item.dt*1000).format('D');
                        item.prevDate = index ? moment(data.list[index - 1].dt*1000).format('D') : null;
                        item.showDate = item.currentDate !== item.prevDate;
                    });
                    return data;
                })
        }
    }
}