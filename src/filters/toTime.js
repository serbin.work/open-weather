import moment from 'moment'

export default (ms) => {
    return moment(ms).format('HH:mm');
}