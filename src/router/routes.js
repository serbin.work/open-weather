import Dashboard from "../components/dashboard/Dashboard";
import Favourites from "../components/favourites/Favourites";
import NotFound from "../views/NotFound";
import CurrentWeather from "../components/dashboard/CurrentWeather";

export default [
    {
        path: '/',
        component: Dashboard,
        children: [
            {
                path: '',
                component: CurrentWeather
            },
            {
                path: 'favourites',
                component: Favourites
            },
        ]
    },
    {
        path: '/favourites',
        component: Favourites,
    }
]