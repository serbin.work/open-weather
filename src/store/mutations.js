export default {
    set(state, payload) {
        state[payload.key] = payload.value;
    },
    removeFavourite(state, index) {
        state.favourites.splice(index, 1);
    }
}